<?php

/*
Plugin Name:  Amelung Developers Handbook
Description:  My Custom Post Type to record steps required to setup a developer's environment
Version:      0.1
Author:       Chris Amelung
Author URI:   https://chrisamelung.com
License:      GPL2
*/


function create_developers_handbook() {
	register_post_type( 'developers_handbook',
		array(
			'labels' => array(
				'name'               => 'Developers Handbook',
				'singluar name'      => 'Developer Handbook',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Handbook Note',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Handbook Note',
				'new_item'           => 'New Handbook Note',
				'view'               => 'View',
				'view_item'          => 'View Handbook Note',
				'search_items'       => 'Search Developers Handbook',
				'not_found'          => 'No Handbook Notes found',
				'not_found_in_trash' => 'No Handbook Notes found in trash',
				'parent'             => 'Parent Handbook Note',
			),
			'public'              => true,
			'show_ui'             => true,
			'show_in_nav_menus'   => true,
			'menu_position'       => 10,
			'supports'            => array(
										'title',
										'editor',
										'comments',
										'revisions',
										'custom_fields',
								    ),
			'exclude_from_search' => false,
			'taxonomies'          => array( '' ),
			'has_archive'         => true,
		)
	);
}
add_action( 'init', 'create_developers_handbook' );

function create_developers_handbook_taxonomies() {
	register_taxonomy(
		'developers_handbook_notes',
		'developers_handbook',
		array(
			'labels'        => array(
				'name'            => 'Handbook Notes',
				'add_new_item'    => 'Add New Note',
				'new_item_name'   => 'New Handbook Note',
			),
			'show_ui'       => true,
			'show_tagcloud' => false,
			'hierarchical'  => false,
		)
	);
}
add_action( 'init', 'create_developers_handbook_taxonomies' );



